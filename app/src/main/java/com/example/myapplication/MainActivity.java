package com.example.myapplication;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    EditText height,weight,ft;
    ProgressBar pbarResult;
    TextView eW,eH,up,down,adjusted,lblBmi;
    double w,h;
    String we,he,wP,hP;
    double feets;
    double adjust;
    int progressStatus = 0;
    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        weight=(EditText)findViewById(R.id.txtWeight);
        height=(EditText)findViewById(R.id.txtHeight);
        eW=(TextView) findViewById(R.id.txtErrorWeight);
        eH=(TextView) findViewById(R.id.txtErrorHeight);
        ft=(EditText)findViewById(R.id.txtHeightFt);
        up=(TextView)findViewById(R.id.lblWeightUp);
        down=(TextView)findViewById(R.id.lblWeightDown);
        adjusted=(TextView)findViewById(R.id.txtWeightAdjust);
        lblBmi=(TextView)findViewById(R.id.txtBmi);
        final Button calculateBmi =(Button)findViewById(R.id.btnCalculateBmi);
        final Button aboutBtn ;

        pbarResult =(ProgressBar)findViewById(R.id.progressBar2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "This Feature Still In Development", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
//        mAppBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.nav_home, R.id.nav_about, R.id.nav_info)
//                .setDrawerLayout(drawer)
//                .build();
//        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
//        NavigationUI.setupWithNavController(navigationView, navController);
//        aboutBtn=(Button)findViewById(R.id.nav_about);

//        aboutBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, AboutActivity.class);
//
//                startActivity(intent);
//            }
//        });

        calculateBmi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                down.setVisibility(View.INVISIBLE);
                up.setVisibility(View.INVISIBLE);
                pbarResult.setProgress(0);
                pbarResult.getProgressDrawable().setColorFilter(
                        Color.parseColor("#3F51B5"), android.graphics.PorterDuff.Mode.SRC_IN);

                verify();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_about:
                Intent intent = new Intent(MainActivity.this, AboutActivity.class);

                startActivity(intent);
                return true;

            case R.id.action_help:
                Intent helpintent = new Intent(MainActivity.this, HelpActivity.class);

                startActivity(helpintent);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void verify(){
        if(weight.getText().toString().equals("")){
            eW.setText("Please enter a weight");
//            eW.setTextColor("red");
            if(ft.getText().toString().equals("") && height.getText().toString().equals("")){
                eH.setText("please enter a height");
            }
        }else{
            try{
                convert();
            }catch (Exception e){
                Toast toast1=Toast. makeText(getApplicationContext(),"ex in verify else", Toast. LENGTH_SHORT);
                toast1. show();
            }



        }
    }

    public  void convert(){
//        Toast toast=Toast. makeText(getApplicationContext(),"Convert called", Toast. LENGTH_SHORT);
//        toast. show();
        w=Integer.parseInt(weight.getText().toString());
        h=Integer.parseInt(height.getText().toString());
        feets= (Double.parseDouble(ft.getText().toString())/3.281);
        Toast toast2=Toast. makeText(getApplicationContext(),"feets"+feets, Toast. LENGTH_SHORT);
//        toast2.show();
        hP="in";
        try {
            if( hP.equals("in")){

                h=h/39.37;
                calculate(w,(feets+h));
            }else{
                h=h/100;
                calculate(w,(feets+h));
            }
        }catch (Exception e){
            Toast toast1=Toast. makeText(getApplicationContext(),"ex in convert", Toast. LENGTH_SHORT);
            toast1. show();
        }



    }

    public void adjustCalculate(double x){

        double wei;
        if(x<25){
            wei =25-x;
            up.setVisibility(View.VISIBLE);
        }else {
            wei=x-25;
            down.setVisibility(View.VISIBLE);
        }

        adjust=wei*(feets+h)*(feets+h);
        adjusted.setText(new DecimalFormat("#.0").format(adjust));


    }

    public void calculate(double w,double h){

        double bmi=w/(h*h);
//        Toast.makeText(MainActivity.this,"bmi is : "+bmi,Toast.LENGTH_SHORT).show();
        lblBmi.setText(new DecimalFormat("#.0").format(bmi));
        double x=((bmi-15)/15)*100;
        if(bmi<=18.5){
            pbarResult.getProgressDrawable().setColorFilter(
                    Color.parseColor("#0A0EC9"), android.graphics.PorterDuff.Mode.SRC_IN);
        }else if(18.5<bmi && bmi<=25.0){
            pbarResult.getProgressDrawable().setColorFilter(
                    Color.parseColor("#5FFF3B"), android.graphics.PorterDuff.Mode.SRC_IN);
        }else{
            pbarResult.getProgressDrawable().setColorFilter(
                    Color.parseColor("#E00606"), android.graphics.PorterDuff.Mode.SRC_IN);
        }


        adjustCalculate(Double.parseDouble(new DecimalFormat("#.0").format(bmi)));
        showProgressBar(x);




    }
    public  void showProgressBar(final double x){
//        progressStatus=x;
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < x) {
                    progressStatus += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            pbarResult.setProgress(progressStatus);
//                            textView.setText(progressStatus+"/"+progressBar.getMax());
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }


}